class Adaptee:
    def GetRequest(self):
        return "Request from the client"


class Target:
    def Request(self):
        pass


class Adapter(Target):
    def __init__(self, adaptee):
        self.adaptee = adaptee
    def Request(self):
        return "This is " + self.adaptee.GetRequest()


adaptee = Adaptee()
target = Adapter(adaptee)
print(target.Request())