class ComputerBuilder:
    def SetMonitor(self):
        pass
    def SetMouse(self):
        pass
    def SetKeyboard(self):
        pass
    def SetTower(self):
        pass
    def SetPrinter(self):
        pass


class Computer:
    monitor = ''
    mouse = ''
    keyboard = ''
    tower = ''
    printer = ''


class ComputerABuiler(ComputerBuilder):
    def __init__(self):
        self.computer = Computer()

    def SetMonitor(self):
        self.computer.monitor = 'Single'
    def SetMouse(self):
        self.computer.mouse = 'Regular'
    def SetKeyboard(self):
        self.computer.keyboard = 'Standard'
    def SetTower(self):
        self.computer.tower = '16 GB RAM'
    def SetPrinter(self):
        self.computer.printer = 'HP Laserject 5000'
    def GetComputer(self):
        return self.computer


class ComputerBBuilder(ComputerBuilder):
    def __init__(self):
        self.computer = Computer()

    def SetMonitor(self):
        self.computer.monitor = 'Dual'
    def SetMouse(self):
        self.computer.mouse = 'Gaming'
    def SetKeyboard(self):
        self.computer.keyboard = 'Standard'
    def SetTower(self):
        self.computer.tower = '64 GB RAM'
    def SetPrinter(self):
        self.computer.printer = 'HP Laserjet 7000'
    def GetComputer(self):
        return self.computer


class ComputerCreator:
    def __init__(self, computer_builder):
        self.computer_builder = computer_builder

    def CreateComputer(self):
        self.computer_builder.SetMonitor()
        self.computer_builder.SetMouse()
        self.computer_builder.SetKeyboard()
        self.computer_builder.SetTower()
        self.computer_builder.SetPrinter()

    def GetComputer(self):
        return self.computer_builder.GetComputer()


computer_a_creator = ComputerCreator(ComputerABuiler())
computer_a_creator.CreateComputer()
print('Computer A: ')
print(computer_a_creator.GetComputer().monitor)
print(computer_a_creator.GetComputer().mouse)
print(computer_a_creator.GetComputer().keyboard)
print(computer_a_creator.GetComputer().tower)
print(computer_a_creator.GetComputer().printer)

print('\n')

computer_b_creator = ComputerCreator(ComputerBBuilder())
computer_b_creator.CreateComputer()
print('Computer B: ')
print(computer_b_creator.GetComputer().monitor)
print(computer_b_creator.GetComputer().mouse)
print(computer_b_creator.GetComputer().keyboard)
print(computer_b_creator.GetComputer().tower)
print(computer_b_creator.GetComputer().printer)