class Facade:
    def __init__(self, subsystem1, subsystem2):
        self.subsystem1 = subsystem1
        self.subsystem2 = subsystem2

    def Operation(self):
        result = 'Facade initializes subsystems: \n'
        result += self.subsystem1.Operation1()
        result += self.subsystem2.Operation1()
        result += 'Facade orders subsystems to perform actions: \n'
        result += self.subsystem1.Operation2()
        result += self.subsystem2.Operation2()
        return result


class Subsystem1:
    def Operation1(self):
        return '\t Subsystem 1 - Operation 1 \n'

    def Operation2(self):
        return '\t Subsystem 1 - Operation 2 \n'


class Subsystem2:
    def Operation1(self):
        return '\t Subsystem 2 - Operation 1 \n'

    def Operation2(self):
        return '\t Subsystem 2 - Operation 2 \n'


class Client:
    def ClientCode(self, facade):
        print(facade.Operation())


system1 = Subsystem1()
system2 = Subsystem2()
facade = Facade(system1, system2)
client = Client()
client.ClientCode(facade)
