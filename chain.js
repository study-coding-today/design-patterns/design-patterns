class HandlerBase {
    constructor(nextHandler, question) {
        this.nextContestant = nextHandler;
        this.question = question;
    }

    HandleRequest() {}
}

class ContestantOne extends HandlerBase {
    constructor(nextHandler, question) {
        super(nextHandler, question);
        this.nextContestant = nextHandler;
        this.question = question;
    }

    HandleRequest() {
        console.log("Question : " + this.question);
        console.log("---------------------------");
        console.log("Waiting for contestant one to respond");
        console.log("\t no response from contestant one....");
        this.nextContestant.HandleRequest();
    }
}

class ContestantTwo extends HandlerBase {
    constructor(nextHandler, question) {
        super(nextHandler, question);
        this.nextContestant = nextHandler;
        this.question = question;
    }

    HandleRequest() {
        console.log("Waiting for contestant two to respond");
        console.log("\t no response from contestant two...");
        this.nextContestant.HandleRequest();
    }
}

class ContestantThree extends HandlerBase {
    constructor(nextHandler, question) {
        super(nextHandler, question);
        this.nextContestant = nextHandler;
        this.question = question;
    }

    HandleRequest() {
        console.log("Waiting for contestant one to respond");
        console.log("\t no response from contestant three as well....");
    }
}

class ContextObject {
    constructor(question) {
        this.question = question;
    }
}

let question = new ContextObject("Who was the first president of the United States?");
let contestantThree = new ContestantThree(null, question);
let contestantTwo = new ContestantTwo(contestantThree, question);
let contestantOne = new ContestantOne(contestantTwo, question);
contestantOne.HandleRequest();