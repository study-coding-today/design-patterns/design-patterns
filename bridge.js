class Message {
    constructor(subject, body) {
        this.subject = subject;
        this.body = body;
    }

    send(sender) {}
}

class SystemMessage extends Message {
    Send(sender) {
        this.sender = sender;
        this.sender.SendMessage(this.subject, this.body);
    }
}

class UserMessage extends Message {
    constructor(subject, body, userComments) {
        super(subject, body);
        this.userComments = userComments;
    }

    Send(sender) {
        this.sender = sender;
        let fullBody = this.body + " " + " - User Comments" + " " + this.userComments;
        this.sender.SendMessage(this.subject, fullBody);
    }
}

class Sender {
    SendMessage(subject, body) {}
}

class FacebookSender extends Sender {
    SendMessage(subject, body) {
        console.log("Facebook - " + subject + " - " + body);
    }
}

class TwitterSender extends Sender {
    SendMessage(subject, body) {
        console.log("Twitter - " + subject + " - " + body);
    }
}

class InstagramSender extends Sender {
    SendMessage(subject, body) {
        console.log("Instagram - " + subject + " - " + body);
    }
}

let facebookSender = new FacebookSender();
let twitterSender = new TwitterSender();
let instagramSender = new InstagramSender();

let message = new SystemMessage("Test Message", "Hello World");

message.Sender = facebookSender;
message.Send(message.Sender);

message.Sender = twitterSender;
message.Send(message.Sender);

message.Sender = instagramSender;
message.Send(message.Sender);

let userMessage = new UserMessage("Test Message", "Hello World", "Hi");

userMessage.Sender = facebookSender;
userMessage.Send(userMessage.Sender);