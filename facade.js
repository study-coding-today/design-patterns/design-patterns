class Facade {
    constructor(subsystem1, subsystem2) {
        this.subsystem1 = subsystem1;
        this.subsystem2 = subsystem2;
    }

    Operation() {
        let result = 'Facade initializes subsystems: \n';
        result += this.subsystem1.Operation1();
        result += this.subsystem2.Operation1();
        result += 'Facade orders subsystems to perform actions: \n';
        result += this.subsystem1.Operation2();
        result += this.subsystem2.Operation2();
        return result;
    }
}

class Subsystem1 {
    Operation1() {
        return '\t Subsystem 1 - Operation 1';
    }

    Operation2() {
        return '\t Subsystem 1 - Operation 2';
    }
}

class Subsystem2 {
    Operation1() {
        return '\t Subsystem 2 - Operation 1 \n';
    }

    Operation2() {
        return '\t Subsystem 2 - Operation 2 \n';
    }
}

class Client {
    ClientCode(facade) {
        console.log(facade.Operation());
    }
}

let system1 = new Subsystem1();
let system2 = new Subsystem2()
let facade = new Facade(system1, system2);
let client = new Client();
client.ClientCode(facade);