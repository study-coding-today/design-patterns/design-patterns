class PlayingCard:
    type = ''
    suit = ''


class HoylePlayingCard(PlayingCard):
    def __init__(self, value, suit):
        self.type = 'Hoyle'
        self.value = value
        self.suit = suit


class CongressPlayingCard(PlayingCard):
    def __init__(self, value, suit):
        self.type = 'Congress'
        self.value = value
        self.suit = suit


class CardFactory:
    def GetPlayingCard(self):
        pass


class HoyleFactory(CardFactory):
    def __init__(self, value, suit):
        self.value = value
        self.suit = suit

    def GetPlayingCard(self):
        return HoylePlayingCard(self.value, self.suit)


class CongressFactory(CardFactory):
    def __init__(self, value, suit):
        self.value = value
        self.suit = suit

    def GetPlayingCard(self):
        return CongressPlayingCard(self.value, self.suit)


factory = None
card = input('Enter the card type you would like to create')
if card.lower() == 'hoyle':
    factory = HoyleFactory(5, 'spades')
elif card.lower() == 'congress':
    factory = CongressFactory(10, 'hearts')
playing_card = factory.GetPlayingCard()
print('Card type: '+ playing_card.type +' Card Value: '+ str(playing_card.value) +' Card suit: '+ playing_card.suit)