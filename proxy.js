class Information {
    GetInformation() {}
}

class SecretInformation extends Information {
    GetInformation() {
        let codeNumber = Math.floor(Math.random() * 99999);
        return codeNumber;
    }
}

class Proxy {
    secretInfo = 0;

    GetSecretInfo(hasSecurityClearance) {
        if (hasSecurityClearance) {
            this.secretInfo = new SecretInformation();
            return this.secretInfo.GetInformation();
        } else {
            return -1;
        }
    }
}

let proxy = new Proxy();
let info = proxy.GetSecretInfo(true);
console.log(info);