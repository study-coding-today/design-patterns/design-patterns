const RED = 'Red';
const GREEN = 'Green';
const BLUE = 'Blue';

class Color {
    Print() {}
}

class Red extends Color {
    Print() {
        console.log('Printing Red');
    }
}

class Green extends Color {
    Print() {
        console.log('Printing Green');
    }
}

class Blue extends Color {
    Print() {
        console.log('Printing Blue');
    }
}

class ColorObjectFactory {
    constructor() {
        this.colors = {};
    }

    TotalObjectsCreated() {
        return Object.keys(this.colors).length;
    }

    GetColor(colorName) {
        let color = null;
        if (this.colors[colorName] !== undefined) {
            color = this.colors[colorName];
        } else {
            switch (colorName) {
                case RED:
                    color = new Red();
                    this.colors[RED] = color;
                    break;
                case GREEN:
                    color = new Green();
                    this.colors[GREEN] = color;
                    break;
                case BLUE:
                    color = new Blue();
                    this.colors[BLUE] = color;
                    break;
            }
        }
        return color;
    }
}

let colorFactory = new ColorObjectFactory();

let color = colorFactory.GetColor(RED);
color.Print();
color = colorFactory.GetColor(GREEN);
color.Print();
color = colorFactory.GetColor(BLUE);
color.Print();
color = colorFactory.GetColor(RED);
color.Print();

let numberOfObjects = colorFactory.TotalObjectsCreated();
console.log('Total objects created = ' + numberOfObjects);