class Car:
    def Type(self):
        pass

    def Price(self):
        pass


class TeslaCar(Car):
    def Type(self):
        return "Tesla"

    def Price(self):
        return 1000000


class VehicleDecorator(Car):
    def __init__(self, vehicle):
        super().__init__()
        self.vehicle = vehicle

    def Type(self):
        return self.vehicle.Type()

    def Price(self):
        return self.vehicle.Price()


class SpecialOffer(VehicleDecorator):
    def __init__(self, vehicle, discount_percentage, offer):
        super().__init__(vehicle)
        self.discount_percentage = discount_percentage
        self.offer = offer

    def Price(self):
        price = super(SpecialOffer, self).Price()
        percentage = 100 - self.discount_percentage
        return float((price * percentage) / 100)


car = TeslaCar()
print('Tesla base price is '+ str(car.Price()))
offer = SpecialOffer(car, 50, '50% discount')
print(offer.offer + ' on Tesla cars, new price is: '+ str(offer.Price()))
