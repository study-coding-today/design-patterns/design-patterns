class Mobile {
    GetAndroidPhone() {}
    GetiOSPhone() {}
}

class Samsung extends Mobile {
    GetAndroidPhone() {
        return new SamsungGalaxy();
    }
    GetiOSPhone() {
        return new SamsungGuru();
    }
}

class Android {
    GetModelDetails() {}
}

class iOS {
    GetModelDetails() {}
}

class SamsungGalaxy extends Android {
    GetModelDetails() {
        return 'Model: Samsung Galaxy - RAM: 2GB - Camera: 13MP';
    }
}

class SamsungGuru extends iOS {
    GetModelDetails() {
        return 'Model: Samsung Guru - RAM: N/A - Camera: N/A';
    }
}

class MobileClient {
    constructor(mobile) {
        this.androidPhone = mobile.GetAndroidPhone();
        this.iOSPhone = mobile.GetiOSPhone();
    }

    GetAndroidPhoneDetails() {
        return this.androidPhone.GetModelDetails();
    }

    GetiOSPhoneDetails() {
        return this.iOSPhone.GetModelDetails();
    }
}

let samsungMobilePhone = new Samsung();
let samsungClient = new MobileClient(samsungMobilePhone);
console.log(samsungClient.GetAndroidPhoneDetails());
console.log(samsungClient.GetiOSPhoneDetails());