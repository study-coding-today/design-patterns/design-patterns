class Adaptee {
    GetRequest() {
        return "Request from the client";
    }
}

class Target {
    Request() {}
}

class Adapter extends Target {
    constructor(adaptee) {
        super(adaptee);
        this.adaptee = adaptee;
    }

    Request() {
        return "This is " + this.adaptee.GetRequest();
    }
}

let adaptee = new Adaptee();
let target = new Adapter(adaptee);
console.log(target.Request());