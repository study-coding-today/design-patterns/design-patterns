class ComputerBuilder {
    SetMonitor() {}
    SetMouse() {}
    SetKeyboard() {}
    SetTower() {}
    SetPrinter() {}
}


class Computer {
    monitor = '';
    mouse = '';
    keyboard = '';
    tower = '';
    printer = '';
}


class ComputerABuilder extends ComputerBuilder {
    constructor() {
        super();
        this.computer = new Computer();
    }

    SetMonitor() {
        this.computer.monitor = 'Single';
    }
    SetMouse() {
        this.computer.mouse = 'Regular';
    }
    SetKeyboard() {
        this.computer.keyboard = 'Standard0';
    }
    SetTower() {
        this.computer.tower = '16 GB RAM';
    }
    SetPrinter() {
        this.computer.printer = 'HP Laserjet 5000';
    }

    GetComputer() {
        return this.computer;
    }
}


class ComputerBBuilder extends ComputerBuilder {
    constructor() {
        super();
        this.computer = new Computer();
    }

    SetMonitor() {
        this.computer.monitor = 'Dual';
    }
    SetMouse() {
        this.computer.mouse = 'Gaming';
    }
    SetKeyboard() {
        this.computer.keyboard = 'Standard';
    }
    SetTower() {
        this.computer.tower = '64 GB RAM';
    }
    SetPrinter() {
        this.computer.printer = 'HP Laserject 7000';
    }

    GetComputer() {
        return this.computer;
    }
}


class ComputerCreator {
    constructor(computerBuilder) {
        this.computerBuilder = computerBuilder;
    }

    CreateComputer() {
        this.computerBuilder.SetMonitor();
        this.computerBuilder.SetMouse();
        this.computerBuilder.SetKeyboard();
        this.computerBuilder.SetTower();
        this.computerBuilder.SetPrinter();
    }

    GetComputer() {
        return this.computerBuilder.GetComputer();
    }
}


let computerACreator = new ComputerCreator(new ComputerABuilder());
computerACreator.CreateComputer();
console.log('Computer A: ');
console.log(computerACreator.GetComputer().monitor);
console.log(computerACreator.GetComputer().mouse);
console.log(computerACreator.GetComputer().keyboard);
console.log(computerACreator.GetComputer().tower);
console.log(computerACreator.GetComputer().printer);

console.log('\n');

let computerBCreator = new ComputerCreator(new ComputerBBuilder());
computerBCreator.CreateComputer();
console.log('Computer B: ');
console.log(computerBCreator.GetComputer().monitor);
console.log(computerBCreator.GetComputer().mouse);
console.log(computerBCreator.GetComputer().keyboard);
console.log(computerBCreator.GetComputer().tower);
console.log(computerBCreator.GetComputer().printer);