class Car {
    constructor(type, price) {
        this.type = type;
        this.price = price;
    }

    Type() {};
    Price() {};
}

class TeslaCar extends Car {
    constructor(type, price) {
        super(type, price);
        this.type = type;
        this.price = price;
    }

    Type() {
        return this.type;
    };

    Price() {
        return this.price;
    };
}

class VehicleDecorator extends Car {
    constructor(vehicle) {
        super(vehicle.price, vehicle.type);
        this.vehicle = vehicle;
    }

    Type() {
        return this.vehicle.type;
    }

    Price() {
        return this.vehicle.price;
    }
}

class SpecialOffer extends VehicleDecorator {
    constructor(vehicle, discountPercentage, offer) {
        super(vehicle);
        this.discountPercentage = discountPercentage;
        this.offer = offer;
    }

    Price() {
        let price = this.vehicle.price;
        let percentage = 100 - this.discountPercentage;
        return parseFloat(String((price * percentage) / 100));
    }
}

let car = new TeslaCar('Tesla', 1000000);
console.log('Tesla base price is ' + car.Price());
let offer = new SpecialOffer(car, 50, '50% discount');
console.log(offer.offer + ' on Tesla cars, new price is: ' + offer.Price());