red = 'Red'
green = 'Green'
blue = 'Blue'


class Color:
    def Print(self):
        pass


class Red(Color):
    def Print(self):
        print('Printing Red')


class Green(Color):
    def Print(self):
        print('Printing Green')


class Blue(Color):
    def Print(self):
        print('Printing Blue')


class ColorObjectFactory:
    def __init__(self):
        self.colors = {}

    def TotalObjectsCreated(self):
        return len(self.colors)

    def GetColor(self, color_name):
        color = None
        if color_name in self.colors.keys():
            color = self.colors[color_name]
        else:
            if color_name == red:
                color = Red()
                self.colors[red] = color
            elif color_name == green:
                color = Green()
                self.colors[green] = color
            elif color_name == blue:
                color = Blue()
                self.colors[blue] = color
            else:
                print('Factory cannot create the object specified')
        return color


colorFactory = ColorObjectFactory()

color = colorFactory.GetColor(red)
color.Print()
color = colorFactory.GetColor(green)
color.Print()
color = colorFactory.GetColor(blue)
color.Print()
color = colorFactory.GetColor(red)
color.Print()

number_of_objects = colorFactory.TotalObjectsCreated()
print('Total objects created = ' + str(number_of_objects))

