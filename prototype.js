function CoursePrototype(proto) {
    this.proto = proto;

    this.clone = function () {
        let course = new Course();
        course.type = proto.type;
        course.id = proto.id;
        return course;
    };
}

function Course(type, id) {
    this.type = type;
    this.id = id;
    this.GetInfo = function () {
        console.log('Course: ' + this.type + ' ' + this.id)
    };
}

let proto = new Course('Programming', 101);
let prototype = new CoursePrototype(proto);
let course = prototype.clone();
course.GetInfo();