class Message:
    def __init__(self, subject, body):
        self.subject = subject
        self.body = body

    def Send(self, sender):
        pass


class SystemMessage(Message):
    def Send(self, sender):
        self.sender = sender
        self.sender.SendMessage(self.subject, self.body)


class UserMessage(Message):
    def __init__(self, subject, body, user_comments):
        super().__init__(subject, body)
        self.user_comments = user_comments

    def Send(self, sender):
        self.sender = sender
        full_body = self.body + ' ' + ' - User Comments' + ' ' + self.user_comments
        self.sender.SendMessage(self.subject, full_body)


class Sender:
    def SendMessage(self, subject, body):
        pass


class FacebookSender(Sender):
    def SendMessage(self, subject, body):
        print('Facebook - ' + subject + ' - ' + body)


class TwitterSender(Sender):
    def SendMessage(self, subject, body):
        print('Twitter - ' + subject + ' - ' + body)


class InstagramSender(Sender):
    def SendMessage(self, subject, body):
        print('Instagram - ' + subject + ' - ' + body)


facebookSender = FacebookSender()
twitterSender = TwitterSender()
instagramSender = InstagramSender()

message = SystemMessage('Test Message', 'Hello World')

message.Sender = facebookSender
message.Send(message.Sender)

message.Sender = twitterSender
message.Send(message.Sender)

message.Sender = instagramSender
message.Send(message.Sender)

userMessage = UserMessage('Test Message', 'Hello World', 'Hi')

userMessage.Sender = facebookSender
userMessage.Send(userMessage.Sender)
