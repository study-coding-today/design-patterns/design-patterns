class PlayingCard {
    type = '';
    suit = '';
}

class HoylePlayingCard extends PlayingCard {
    constructor(value, suit) {
        super();
        this.type = 'Hoyle';
        this.suit = suit;
        this.value = value;
    }
}

class CardFactory {
    GetPlayingCard() {}
}

class HoyleFactory extends CardFactory {
    constructor(value, suit) {
        super();
        this.value = value;
        this.suit = suit;
    }

    GetPlayingCard() {
        return new HoylePlayingCard(this.value, this.suit);
    }
}

let factory = new HoyleFactory(5, 'spades');
let playingCard = factory.GetPlayingCard();
console.log('Card type: ' + playingCard.type + 'Card value: ' + playingCard.value + ' Card suit: ' + playingCard.suit);