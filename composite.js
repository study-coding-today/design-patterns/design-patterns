class Component {
    AddChild() {}
    Traverse() {}
}

class File extends Component {
    constructor(value) {
        super();
        this.value = value;
    }

    AddChild(component) {}

    Traverse() {
        console.log('File: ' + this.value);
    }
}

class Folder extends Component {
    constructor(value) {
        super();
        this.value = value;
        this.componentList = [];
    }

    AddChild(component) {
        this.componentList.push(component);
    }

    Traverse() {
        console.log('Folder: ' + this.value);
        this.componentList.forEach(component => {
            component.Traverse();
        });
    }
}

let folder1 = new Folder('Folder 1 Information');
let folder2 = new Folder('Folder 2 Information');
let file1 = new File('File 1 Information');
let file2 = new File('File 2 Information');
folder2.AddChild(file1);
folder2.AddChild(file2);
let file3 = new File('File 3 Information');
folder1.AddChild(folder2);
folder1.AddChild(file3);
folder1.Traverse();

