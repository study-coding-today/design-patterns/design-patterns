class Component:
    def add_child(self, component):
        pass

    def traverse(self):
        pass


class File(Component):
    def __init__(self, value):
        self.value = value

    def add_child(self, component):
        pass

    def traverse(self):
        print('File: ' + self.value)


class Folder(Component):
    def __init__(self, value):
        self.value = value
        self.component_list = []

    def add_child(self, component):
        self.component_list.append(component)

    def traverse(self):
        print('Folder: ' + self.value)
        for component in self.component_list:
            component.traverse()


folder1 = Folder('Folder 1 Information')
folder2 = Folder('Folder 2 Information')
file1 = File('File 1 Information')
file2 = File('File 2 Information')
folder2.add_child(file1)
folder2.add_child(file2)
file3 = File('File 3 Information')
folder1.add_child(folder2)
folder1.add_child(file3)
folder1.traverse()