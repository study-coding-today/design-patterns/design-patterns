class Mobile:
    def GetAndroidPhone(self):
        pass

    def GetiOSPhone(self):
        pass


class Samsung(Mobile):
    def GetAndroidPhone(self):
        return SamsungGalaxy()

    def GetiOSPhone(self):
        return SamsungGuru()


class Andriod:
    def GetModelDetails(self):
        pass


class iOS:
    def GetModelDetails(self):
        pass


class SamsungGalaxy(Andriod):
    def GetModelDetails(self):
        return 'Model: Samsung Galaxy - RAM: 2GB - Camera: 13MP'


class SamsungGuru(iOS):
    def GetModelDetails(self):
        return 'Model: Samsung Guru - RAM: N/A - Camera: N/A'


class MobileClient:
    def __init__(self, mobile: Mobile):
        self.android_phone = mobile.GetAndroidPhone()
        self.ios_phone = mobile.GetiOSPhone()

    def GetAndroidPhoneDetails(self):
        return self.android_phone.GetModelDetails()

    def GetiOSPhoneDetails(self):
        return self.ios_phone.GetModelDetails()


samsung_mobile_phone = Samsung()
samsung_client = MobileClient(samsung_mobile_phone)
print(samsung_client.GetAndroidPhoneDetails())
print(samsung_client.GetiOSPhoneDetails())
