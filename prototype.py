from abc import ABC
import copy


class Courses_At_Study(ABC):
    def __init__(self):
        self.id = None
        self.type = None

    def get_type(self):
        return self.type

    def get_id(self):
        return self.id

    def set_id(self, id):
        self.id = id

    def clone(self):
        return copy.copy(self)


class P(Courses_At_Study):
    def __init__(self):
        super().__init__()
        self.type = 'Programming Courses'


class A(Courses_At_Study):
    def __init__(self):
        super().__init__()
        self.type = 'Art Courses'


class Courses_At_Study_Cache:
    cache = {}

    @staticmethod
    def get_course(id):
        COURSE = Courses_At_Study_Cache.cache.get(id, None)
        return COURSE.clone()

    @staticmethod
    def load():
        programming = P()
        programming.set_id('1')
        Courses_At_Study_Cache.cache[programming.get_id()] = programming

        art = A()
        art.set_id('2')
        Courses_At_Study_Cache.cache[art.get_id()] = art


Courses_At_Study_Cache.load()

programming = Courses_At_Study_Cache.get_course('1')
print(programming.get_type())

art = Courses_At_Study_Cache.get_course('2')
print(art.get_type())
